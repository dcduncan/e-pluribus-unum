/**
 * Created by niklj_000 on 5/3/2015.
 */
function highlightNavBar(id) {
    console.log(id);
    var embed = document.getElementById("navigationIframe");
    var bullion = embed.contentDocument.getElementById(id);
    bullion.style.backgroundColor="#000000";
    bullion.style.opacity="0.5";
}

function loadUnload(loadId, unloadId) {
    console.log(loadId);
    console.log(unloadId);
    var unload = document.getElementById(unloadId);
    unload.style.backgroundColor="#9aafbd";
    unload.style.opacity="1";

    var load = document.getElementById(loadId);
    load.style.backgroundColor="#000000";
    load.style.opacity="0.5";

    var myStackTable = document.getElementById("myStackTable");
    var bullionTable = document.getElementById("bullionTable");
    var graphAside = document.getElementById("graphAside");
    if (loadId === "myStack") {
        myStackTable.style.visibility="visible";
        bullionTable.style.visibility="visible";
        graphAside.style.visibility="hidden";
    } else {
        myStackTable.style.visibility="hidden";
        bullionTable.style.visibility="hidden";
        graphAside.style.visibility="visible";
    }
}






function showNavigation() {
    if( $("#showNavigation").hasClass( "activeNav" ) ){
        $( "#showNavigation" ).toggleClass( 'activeNav', false);
        document.getElementById("showNavigation").style.visibility = "hidden";
    }
    else{
        document.getElementById("showNavigation").style.visibility = "visible";
        document.getElementById("showNavigation").style.top = 36+"px";
        $( "#showNavigation" ).toggleClass( 'activeNav', true );
    }           

}


function activateImageSwitch1() {

    //$( "#mydiv" ).hasClass( "foo" ) returns true if does
    //$( "#foo" ).toggleClass( className, addOrRemove ); true add, false remove

    if( $("#bone").hasClass( "activeImage" ) ){
        //do nothing
    }
    else{

        $("#imgSpotDayView").show();
        $("#imgSpotMonthView").hide();
        document.getElementById("imgSpotDayView").style.visibility = "visible";
        document.getElementById("imgSpotMonthView").style.visibility = "hidden";
        $( "#bone" ).toggleClass( 'activeImage', true );
        $( "#btwo" ).toggleClass( 'activeImage', false );
        $( "#bone" ).toggleClass( 'customButtonPrimary', true );
        $( "#btwo" ).toggleClass( 'customButtonPrimary', false );
        $( "#bone" ).toggleClass( 'customButtonDefault', false );
        $( "#btwo" ).toggleClass( 'customButtonDefault', true );


    }


}

function activateImageSwitch2() {

    //$( "#mydiv" ).hasClass( "foo" ) returns true if does
    //$( "#foo" ).toggleClass( className, addOrRemove ); true add, false remove

    if( $("#btwo").hasClass( "activeImage" ) ){
        //do nothing
    }
    else{

        $("#imgSpotDayView").hide();
        $("#imgSpotMonthView").show();
        document.getElementById("imgSpotDayView").style.visibility = "hidden";
        document.getElementById("imgSpotMonthView").style.visibility = "visible";
        $( "#bone" ).toggleClass( 'activeImage', false );
        $( "#btwo" ).toggleClass( 'activeImage', true );
        $( "#bone" ).toggleClass( 'customButtonPrimary', false );
        $( "#btwo" ).toggleClass( 'customButtonPrimary', true );
        $( "#bone" ).toggleClass( 'customButtonDefault', true );
        $( "#btwo" ).toggleClass( 'customButtonDefault', false );

    }


}