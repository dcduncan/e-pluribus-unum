$( document ).ready(function() {


//wait two seconds to resize square
setTimeout(function() {

	/*$(".landing-modal img").animate({
		margin: '10px 0 0 -100px',
	    top: '50%',
	    left: '50%'  
	}, 300);*/

	// this commented code is if we include logo 

	/*$(".landing-modal img").css("left","50%");
	$(".landing-modal img").css("top","50%");*/
	$(".landing-modal img").css("visibility", "hidden");

	$(".landing-modal ").css("background","white");

	$(".landing-modal").animate({
		margin: '0 0 0 0',
	    height: '100%',
	    width: '100%',
	    top: '0px',
	    left: '0px'  
	}, 800, function() {

		//set visibility
		$(".landing-modal h3").css("visibility","visible");  
		$(".landing-modal h4").css("visibility","visible");  
		$(".showHomePage").css("visibility","visible");

		//animate elements and divs in
		$(".landing-modal h3").animate({
		    left: '0px',
		}, 500);
		$(".landing-modal h4").animate({
		    left: '0px',
		}, 900);
		$(".showHomePage").animate({
		    left: '0px',
		}, 1300);


	});

}, 1000);


//toggle modal closed
$('.showHomePage').click(function() {

	$(".container").css("visibility","visible");

	$(".landing-modal").animate({
		margin: '-50px 0 0 -50px',
	    height: '0px',
	    width: '0px',
	    top: '50%',
	    left: '50%'     
	}, 500, function() {

		$(".landing-modal h3").css("visibility","hidden");  
		$(".landing-modal h4").css("visibility","hidden");  
		$(".showHomePage").css("visibility","hidden");
		$(".landing-modal img").css("visibility","hidden");
		$("body").css("overflow", "visible");
		$("html").css("overflow", "visible");
	});

});


}); //end of document on ready
