/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    concat: {
      dist: {
        //src: ['jquery-1.11.2.min.js','Chart.js','parse-1.4.2.min.js'],
        src: ['js/parseHelper.js','js/main.js' ],
        dest: 'dist/js/cc.js'
        //dest: 'dist/js/jqueryVelocityChartParse.min.js'
      }
    },
    uglify: {
      dist: {
        src: ['dist/js/cc.js'],
        dest: 'js/cc.min.js'
      }
    },
    cssmin: {
      target: {
        files:[{
          expand: true,
          cwd: 'style',
          src: ['*.css', '!*.min.css'],
          dest: 'style',
          ext: '.min.css'
        }]
      }
    },
    watch: {
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile']
      },
      lib_test: {
        files: '<%= jshint.lib_test.src %>',
        tasks: ['jshint:lib_test', 'qunit']
      }
    }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  //grunt.loadNpmTasks('grunt-contrib-qunit');
  //grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  // Default task.
  grunt.registerTask('default', ['cssmin', 'concat', 'uglify']);

};
