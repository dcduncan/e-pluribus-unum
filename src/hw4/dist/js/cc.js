
//object.purchasedDate
//object.value
var userGraphInfo = {

    'storedData': {},

    getLabels: function(xData){
        var labels = [];
        var tmpString;
        var split;
        console.log("getLabels");
        console.log(xData);
        for( var i = 0; i < xData.length ; ++i){
                tmpString = String(xData[i].purchaseDate);
                split = tmpString.split(" ");
                labels[i] = split[0] + " " + split[1] + " " + split[2];
        }

        return labels;
    },

    getPrice: function(xData){
        var prices = [];
        console.log(xData);
        for( var i = 0; i < xData.length; ++i){
            if( i != 0)
                prices[i] = (xData[i].value ) +  prices[i-1];
            else
                prices[i] = (xData[i].value);
        }

        return prices;

    },

    drawUserInfoGraph : function(){
        //while(platinumData == null || goldData == null || silverData == null);
        var pointStroke = "rgba(255,255,255,0.6)";
        var pointHighlightFill = "grey";
        var pointHighlightStroke = "#fff";

        var data = {
            labels: userGraphInfo.getLabels(userGraphInfo.storedData),
            datasets: [
                {
                    label: "User Total",
                    fillColor: "rgba(104, 206, 222, 0.05)",
                    strokeColor: "#FF6D67",
                    pointColor: "#FF6D67",
                    pointStrokeColor: pointStroke,
                    pointHighlightFill: pointHighlightFill,
                    pointHighlightStroke: pointHighlightStroke,
                    data: userGraphInfo.getPrice(userGraphInfo.storedData)
                },
                /*{
                 label: "1oz Gold",
                 fillColor: "rgba(104, 206, 222, 0.05)",
                 strokeColor: "#9FFF98",
                 pointColor: "#9FFF98",
                 pointStrokeColor: pointStroke,
                 pointHighlightFill: pointHighlightFill,
                 pointHighlightStroke: pointHighlightStroke,
                 data: [100, 110, 120, 90, 102, 135, 115]
                 }*/
            ]
        };

        var options = {

            ///Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines : true,

            //String - Colour of the grid lines
            scaleGridLineColor : "rgba(104, 206, 222, 0.1)",

            //Number - Width of the grid lines
            scaleGridLineWidth : 1,

            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,

            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,

            //Boolean - Whether the line is curved between points
            bezierCurve : true,

            //Number - Tension of the bezier curve between points
            bezierCurveTension : 0.4,

            //Boolean - Whether to show a dot for each point
            pointDot : true,

            //Number - Radius of each point dot in pixels
            pointDotRadius : 4,

            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth : 1,

            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius : 20,

            //Boolean - Whether to show a stroke for datasets
            datasetStroke : true,

            //Number - Pixel width of dataset stroke
            datasetStrokeWidth : 2,

            //Boolean - Whether to fill the dataset with a colour
            datasetFill : true,

            //String - A legend template
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",

            responsive: true,

            maintainAspectRatio: false,


        };

        if (userGraphInfo.storedData.length === 0) {
            document.getElementById("poor-span").innerHTML = "YOU ARE TOO POOR FOR COINS!";
        } else {
            document.getElementById("total-chart").style.visibility = "visible";
            var ctx = document.getElementById("total-chart").getContext("2d");
            var coinChart = new Chart(ctx).Line(data, options);
            coinChart.update();
        }
        document.getElementById("chart-is-loading").style.visibility = "hidden";
    },
};


/**
 * Created by niklj_000 on 5/25/2015.
 */

/**
 * Returns the boolean if the user has a dark theme preference.
 * @returns {boolean}
 */
function getDarkThemePreference() {
    Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
    Parse.User.current().fetch().then(function (user) {
        if (user.get("darkTheme")) {
            var yourElement = document.getElementById('styleSheetCheck');
            yourElement.setAttribute('href', './style/style.css');
            //document.getElementById('chart-is-loading').src = "./img/ajax-loader.gif"
        }
        console.log("Updated.");
    });
}

/**
 * Sets the users dark theme preference
 * @param wantsDarkTheme
 */
function setDarkThemePreference(wantsDarkTheme) {
    Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
    Parse.User.current().fetch().then(function (user) {
        user.set("darkTheme", wantsDarkTheme);
        console.log(wantsDarkTheme);
        user.save();
    });
}

/**
 * Returns the boolean if the user has a light theme preference.
 * @returns {boolean}
 */
function getLightThemePreference() {
    return !getDarkThemePreference();
}

/**
 * Sets the users light theme preference
 * @param wantsLightTheme
 */
function setLightThemePreference(wantsLightTheme) {
    setDarkThemePreference(!wantsLightTheme);
}

/**
 * Sets the users  password to the provided password
 * @param password
 */
function setPassword(password) {
    Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
    Parse.User.current().fetch().then(function (user) {
        user.set("password", password);
        console.log(password);
        user.save();
        alert("Password updated.");
    });
}

/**
 * Sets the users email to the provided email
 * @param email
 */
function setEmail(email) {
    Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
    Parse.User.current().fetch().then(function (user) {
        user.set("email", email);
        user.set("username", email);
        console.log(email);
        user.save();
        alert("Email updated.");
    });
}

/**
 *
 * @param designator, The title of the content that would like to be retrieved ie: metal, type...
 * @param specifier, The actual value of the thing to compare with the designator
 * @param detailsToGrab, an array of Strings with all of the details to grab about each of the metals
 * @return an array of objects that contain all of the data to populate the table in the specific gold, silver and platinum pages.
 */
function getBullion(designator, specifier, detailsToGrab) {
    Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
    Parse.User.current().fetch().then(function (user) {
        var userName = user.get("username");
        var Bullion = Parse.Object.extend("Bullion");
        var query = new Parse.Query(Bullion);
        query.equalTo("userName", userName);
        query.equalTo(designator, specifier);
        query.ascending("purchaseDate");
        query.find({
            success: function(results) {
                formattedResults = [];
                for (var i = 0; i < results.length; ++i) {
                    var object = results[i];

                    formattedResults[i] = {};

                    for (var j = 0; j < detailsToGrab.length; ++j) {
                        if(j == 0 && object.get(detailsToGrab[j]) != null)
                            formattedResults[i][detailsToGrab[j]] = object.get(detailsToGrab[j]).url();
                        else
                            formattedResults[i][detailsToGrab[j]] = object.get(detailsToGrab[j]);
                    }
                }
                //get info for printing graph

                userGraphInfo.storedData = formattedResults;
                userGraphInfo.drawUserInfoGraph();
                populateBullionTable(formattedResults);
                return formattedResults;
            },
            error: function(error) {
                console.log("Error getting all bullion: " + error.code + " -> " + error.message);
                return undefined;
            }
        });
    });
}

/**
 * Retrieves all of the details about a specific bullion for the current user
 * @param type, The string of the type of bullion that you would like retrieved.
 * @return an array of objects that contain all of the data to populate the detail page for the specific bullion.
 */
function getBullionsForItemDetails(metal) {
    var detailsToGrab = ["picture", "metal", "type", "purchaseDate", "quantity", "premium", "unitPrice", "percent", "weight", "gu", "ozt", "totalOZT", "value"];
    return getBullion("metal", metal, detailsToGrab);
}

/**
 *
 * @param metal, The string of the type of metal that you would like retrieved; takes 3 values: gold, silver, and platinum for gold, silver and platinum respectively
 * @return an array of objects that contain all of the data to populate the table in the specific gold, silver and platinum pages.
 */
function getBullionForTable(metal) {
    var detailsToGrab = ["picture", "type", "quantity", "weight", "percent", "value"];
    return getBullion("metal", metal, detailsToGrab);
}


function populateBullionTable(bullionList) {
    var bullionTable = document.getElementById("bullionTableBody");
    var totalValue = 0;

    for (i = 0; i < bullionList.length; i++) {
        var tr = document.createElement('TR');
        tr.setAttribute("id", "coin"+i);
        tr.setAttribute("onclick", "getClickedElement("+i+");");


        var imgTD = document.createElement("TD");
        imgTD.className = "stack_img_col";

        var imgDiv = document.createElement("DIV");
        imgDiv.className = "coin_mini";

        imgTD.appendChild(imgDiv);

        var infoAnchor = document.createElement("A");
        infoAnchor.setAttribute("href", "coinInfo.html");

        var detailsToGrab = ["picture", "type", "quantity", "weight", "percent", "value"];
        for (j = 0; j < detailsToGrab.length; j++) {
            var td = document.createElement('TD');
            if(j == 1) {
                td.appendChild(infoAnchor);
            }

            if(j == 0) {
                td = imgTD;
                if(bullionList[i][detailsToGrab[j]] != null) {
                    imgURL = bullionList[i][detailsToGrab[j]];
                    var img = document.createElement("IMG");
                    img.src = imgURL;
                    img.className = "coin_mini";
                    imgDiv.appendChild(img);
                }
            }
            else if(bullionList[i][detailsToGrab[j]] != null) {
                td.appendChild(document.createTextNode(bullionList[i][detailsToGrab[j]]));
            }
            tr.appendChild(td);
        }
        bullionTable.appendChild(tr);
        totalValue += bullionList[i]["value"];
    }

    setTimeout(function(){
         document.getElementById("loader").setAttribute("visibility", "none");
         document.getElementById("total_value").innerHTML = "$" + totalValue;
        }, 500);

    // For searching capability
    var $rows = $('#bullionTableBody tr');
    $('#searchCoins').keyup(function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

    $rows.show().filter(function() {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();

    });
}

    // get coin index from table and grab corresponding coin in array
    function getClickedElement(index) {
        console.log("Index: " + index);
        var coin = formattedResults[index];
        setCoinInfo(coin);
    }

    function setCoinInfo(coin) {
        //"picture", "metal", "type", "purchaseDate", "quantity", "premium", "unitPrice", "percent", "weight", "gu", "ozt", "totalOZT", "value"
        localStorage.setItem("coin_info_coin", JSON.stringify(coin));

        // format of the date in stringify is weird...
        localStorage.setItem("purchase_date_info", coin["purchaseDate"]);

        // go to coinInfo page
        window.location.href = "coinInfo.html";
    }

//only initialized once to tell whether we are in view or edit mode
var isViewMode = true;
function toggleEditViewMode() {
    //toggles view mode
    isViewMode = !isViewMode;

    //flips all of the readonly items to be editable or vice versa
    var inputsTags = document.getElementsByClassName("toggleEditable");
    for (var i = 0; i < inputsTags.length; ++i) {
        inputsTags[i].readOnly = !inputsTags[i].readOnly;
    }

    //Grabs the three buttons that change
    var editCancelButton = document.getElementById("editCancelButton");
    var deleteButton = document.getElementById("deleteButton");
    var saveButton = document.getElementById("save");

    //Set the image and visibility depending on the mode
    if (isViewMode) {
        editCancelButton.src="img/pencil.png";
        editCancelButton.title="Edit";
        deleteButton.style.visibility="hidden";
        saveButton.style.visibility="hidden";
    } else {
        editCancelButton.src="img/cancel.png";
        editCancelButton.title="Cancel";
        deleteButton.style.visibility="visible";
        saveButton.style.visibility="visible";
    }
}

function validateBullionUpdate() {
    var errors = {};
    errors.errorMessage = "";
    var quantity = document.getElementById("qty_info");
    var premium = document.getElementById("premium_info");
    var unitPrice = document.getElementById("unit_info");

    if (quantity.value.length < 1) {
        errors.errorMessage += "Please enter a quantity.\n";
        errors.quantityError = true;
    } else if (isNaN(quantity.value)) {
        errors.errorMessage += "The quantity must be a numeric value.\n";
        errors.quantityError = true;
    } else if (quantity.value < 0) {
        errors.errorMessage += "The quantity must be a positive integer.\n";
        errors.quantityError = true;
    }

    if (premium.value.length < 1) {
        errors.errorMessage += "Please enter a premium.\n";
        errors.premiumError = true;
    } else if (isNaN(premium.value)) {
        errors.errorMessage += "The premium must be a numeric value.\n";
        errors.premiumError = true;
    } else if (premium.value < 0) {
        errors.errorMessage += "The premium must be a positive integer.\n";
        errors.premiumError = true;
    }

    if (unitPrice.value.length < 1) {
        errors.errorMessage += "Please enter a unit price.\n";
        errors.unitPriceError = true;
    } else if (isNaN(unitPrice.value)) {
        errors.errorMessage += "The unit price must be a numeric value.\n";
        errors.unitPriceError = true;
    } else if (unitPrice.value < 0) {
        errors.errorMessage += "The unit price must be a positive number.\n";
        errors.unitPriceError = true;
    }

    if (errors.quantityError) {
        quantity.style.background = '#AA0000';
    } else {
        quantity.style.background = '#4c4c4c';
    }

    if (errors.premiumError) {
        premium.style.background = '#AA0000';
    } else {
        premium.style.background = '#4c4c4c';
    }

    if (errors.unitPriceError) {
        unitPrice.style.background = '#AA0000';
    } else {
        unitPrice.style.background = '#4c4c4c';
    }

    errors.error = errors.quantityError || errors.premiumError || errors.unitPriceError;
    if (errors.error) {
        alert(errors.errorMessage);
    }
    return !errors.error;
}

function updateBullion() {
    if (validateBullionUpdate()) {
        Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
        Parse.User.current().fetch().then(function (user) {
            var userName = user.get("username");
            var Bullion = Parse.Object.extend("Bullion");
            var query = new Parse.Query(Bullion);
            query.equalTo("userName", userName);
            query.equalTo("type", document.getElementById("type_info").innerHTML);
            query.find({
                success: function (results) {
                    var result = results[0];
                    result.set("quantity", parseInt(document.getElementById("qty_info").value));
                    result.set("premium", parseInt(document.getElementById("premium_info").value));
                    result.set("unitPrice", parseInt(document.getElementById("unit_info").value));
                    result.set("value", parseInt(document.getElementById("totalStrong").innerHTML));

                    result.save(null, {
                        success: function (updatedObject) {
                            alert("Update complete!");
                            toggleEditViewMode();
                        },

                        error: function (error) {
                            alert("An error occurred while trying to save you update :,(");
                        }
                    })
                },
                error: function (error) {
                    console.log("Error getting all bullion: " + error.code + " -> " + error.message);
                    return undefined;
                }
            });
        });
    }
}

function destoyBullion() {
    Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
    Parse.User.current().fetch().then(function (user) {
        var userName = user.get("username");
        var Bullion = Parse.Object.extend("Bullion");
        var query = new Parse.Query(Bullion);
        query.equalTo("userName", userName);
        query.equalTo("type", document.getElementById("type_info").innerHTML);
        query.find({
            success: function(results) {
                results[0].destroy({
                   success: function(deadObject) {
                        alert("Successfully deleted the item.");
                        document.location = document.getElementById("backLink").getAttribute("href");
                   },

                    error: function(error) {
                        alert("An error occurred while trying to delete your bullion :,(");
                    }
                });
            },

            error: function(error) {
                alert("An error occurred while trying to delete your bullion :,(");
            }
        });
    });
}

/*
options = {
    metal: String,
    type: String,
    picture: ???,
    purchase: Date,
    premium: Number,
    Unit Price: Number
    }

create Bullion(options, picture)
getBullionForUser(metal)
deleteBullion(type, quarnity)
editBullion(options, picture)
*/

/*
metal: $('#metal').val(),
            type: $('#type').val(),
            purchaseDate: $('#purchaseDate').val(),
            premium: $('#value').val(),
            unitPrice: $('#unitPrice').val()
*/

function createBullion(picFile, email) {
        Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
        var newBullion = Parse.Object.extend("Bullion")
        var newB = new newBullion();
        newB.save({
            userName: email,
            metal: $('#metal').val(),
            type: $('#type').val(),
            quantity: parseInt($('#quantity').val()),
            purchaseDate: checkDate($('#purchaseDate').val()),
            premium: parseFloat($('#value').val()),
            unitPrice: parseFloat($('#unitPrice').val()),
            picture: picFile,
            percent: parseFloat($('#percentValue').text()),
            ozt: parseFloat($('#oztValue').text()),
            total: parseFloat($('#bigTotal').text()),
            weight: parseFloat($('#grams').text()),
            value: parseFloat($('#bigTotal').text()),
            gu: parseFloat($('#totalValue').text()),
            totalOZT: parseFloat($('#oztValue').text())

        },{
        success: function(newB){
            //alert("it went through");
            document.location = $('#metal').val() + "Home.html";
        },
        error: function(newB,error){
            alert('post failed, fields all need to be filled correctly');
        }});
    }

function createBullionNoPic(email) {
    Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
    var newBullion = Parse.Object.extend("Bullion")
    var newB = new newBullion();
    newB.save({
        userName: email,
        metal: $('#metal').val(),
        type: $('#type').val(),
        quantity: parseInt($('#quantity').val()),
        purchaseDate: checkDate($('#purchaseDate').val()),
        premium: parseFloat($('#value').val()),
        unitPrice: parseFloat($('#unitPrice').val()),
        percent: parseFloat($('#percentValue').text()),
        ozt: parseFloat($('#oztValue').text()),
        total: parseFloat($('#bigTotal').text()),
        weight: parseFloat($('#grams').text()),
        value: parseFloat($('#bigTotal').text()),
        gu: parseFloat($('#totalValue').text()),
        totalOZT: parseFloat($('#oztValue').text())

    },{
        success: function(newB){
            //alert("it went through");
            document.location = $('#metal').val() + "Home.html";
        },
        error: function(newB,error){
            alert('post failed, fields all need to be filled correctly');
        }});
}


//used for date validation.
function checkDate(str) {

        var matches = str.match(/(\d{1,2})[- \/](\d{1,2})[- \/](\d{4})/);
        if (!matches) return;

        // convert pieces to numbers
        // make a date object out of it
        var month = parseInt(matches[1], 10);
        var day = parseInt(matches[2], 10);
        var year = parseInt(matches[3], 10);
        var date = new Date(year, month - 1, day);
        if (!date || !date.getTime()) return;

        // make sure we didn't have any illegal
        // month or day values that the date constructor
        // coerced into valid values
        if (date.getMonth() + 1 != month ||
            date.getFullYear() != year ||
            date.getDate() != day) {
                return;
            }
    return(date);
    }

function loadFooter(){
	document.write("    <footer>");
	document.write("        &copy; 2015 Numismatics");
	document.write("    <\/footer> ");
};

/**
 * Returns true if a valid email
 * @param email
 */
function validateEmail(email) {
    var errors = {};
    var emailRegex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if (email.length == 0) {
        errors.errorMessage = "Please enter an email."
        errors.hasMessage = true;
    } else if (email.indexOf("@") < 0) {
        errors.errorMessage = "Email needs to contain an '@.'";
        errors.hasMessage = true;
    } else if (!emailRegex.test(email)) {
        errors.errorMessage = "Please enter a valid email format.";
        errors.hasMessage = true;
    }

    if (errors.hasMessage) {
        document.getElementById("user_email").style.background = '#AA0000';
        alert(errors.errorMessage);
    } else {
        document.getElementById("user_email").style.background = '#4c4c4c';
    }

    return !errors.hasMessage;
}

/**
 * Returns true if valid.
 * @param password
 * @param passwordConfirm
 * @returns {boolean}
 */
function validateChangePassword(password, passwordConfirm) {
    var errors = {};
    if (password !== passwordConfirm) {
        errors.errorMessage = "Passwords must match.";
        errors.hasError = true;
    } else if (password.length < 8) {
        errors.errorMessage = "Password must be atleast 8 characters.";
        errors.hasError = true;
    }

    if (errors.hasError) {
        document.getElementById("user_pCheck").style.background = '#AA0000';
        alert(errors.errorMessage);
    } else {
        document.getElementById("user_pCheck").style.background = '#4c4c4c';
    }

    return !errors.hasError;
}

function calculateMarket() {
    ms = {};
    ms.today = new Date();
    ms.date = ms.today.toString().split(" ");
    ms.date[4] = ms.date[4].split(":");
    ms.currentHours = parseInt(ms.date[4][0]);
    ms.currentMinutes = parseInt(ms.date[4][1]);
    ms.currentTime = parseInt(ms.date[4][0] + ms.date[4][1]);
    ms.timeRemaining = 0;
    ms.timeRemainingHours = 0;
    ms.timeRemainingMinutes = 0;
    ms.dayOfWeek = ms.date[0];
    ms.isMarketClosed = ms.currentTime < 630 || ms.currentTime > 1830 || ms.dayOfWeek == "Sun" || ms.dayOfWeek == "Sat";

    if (ms.isMarketClosed) {
        document.getElementById("marketOpenClosed").innerHTML = "Market is Closed";
        if (ms.currentTime < 630) {
            ms.timeRemainingHours = 6 - ms.currentHours;
            ms.timeRemainingMinutes = 30 - ms.currentMinutes;
        } else {
            ms.timeRemaining = 0;
            if (ms.currentTime > 1830) {
                ms.timeRemainingHours = 23 - ms.currentHours;
                ms.timeRemainingMinutes = 59 - ms.currentMinutes;
                ms.timeRemainingHours += 6;
                ms.timeRemainingMinutes += 30;
            }
        }

        if (ms.dayOfWeek == "Sat" || ms.dayOfWeek == "Sun") {

            if (ms.currentTime > 630) {
                ms.timeRemainingHours = 24 - ms.currentHours;
                ms.timeRemainingMinutes = 30 - ms.currentMinutes;
                ms.timeRemainingHours += 6;
                ms.timeRemainingMinutes += 30;
            } else {
                ms.timeRemainingHours = 6 - ms.currentHours;
                ms.timeRemainingMinutes = 30 - ms.currentMinutes;
            }

            if (ms.dayOfWeek == "Sat") {
                ms.timeRemainingHours += 24;
            }

            if (ms.currentTime < 630) {
                ms.timeRemainingHours += 24;
            }

            ms.timeRemainingHours--;
            ms.timeRemainingMinutes += 30;
            if (ms.timeRemainingMinutes > 60) {
                ms.timeRemainingMinutes = ms.timeRemainingMinutes % 60;
                ++ms.timeRemainingHours;
            }
        }

        if (ms.timeRemainingMinutes < 0) {
            ms.timeRemaining *= -1;
        }

        document.getElementById("marketTime").innerHTML = "Opens in " + ms.timeRemainingHours + "h " + ms.timeRemainingMinutes + "min.";

    } else {
        document.getElementById("marketOpenClosed").innerHTML = "Market is Open";
        ms.timeRemainingHours = 18 - ms.currentHours;
        if (ms.currentMinutes > 30) {
            ms.timeRemainingMinutes = ms.timeRemainingMinutes - 30;
            ms.timeRemainingHours--;
        } else {
            ms.timeRemainingMinutes = 30 - ms.timeRemainingMinutes;
        }

        if (ms.timeRemainingMinutes < 0) {
            ms.timeRemaining *= -1;
        }

        document.getElementById("marketTime").innerHTML = "Closes in " + ms.timeRemainingHours + "h " + ms.timeRemainingMinutes + "min.";
    }
};
/* * * * * * * * * * * * * *
 *                         *
 *       Cache Data        *
 *                         *
 * * * * * * * * * * * * * */
var lc = {

        supports_html5_storage: function() {
          try {
            return 'localStorage' in window && window['localStorage'] !== null;
          } catch (e) {
            return false;
          }
        },

        'bCacheSupported': false,

        oldDataUpdate: function(xData, dayOffset, objectName){
                var cachedDate = xData[0][0];           //current cached date
                var split = cachedDate.split("-");      //split cached integers saved as strings
                //get a string format of our cached date
                var oldDay = new Date(parseInt(split[0]),
                        parseInt(split[1])-1,parseInt(split[2]));
                var splitOld = String(oldDay).split(" ");
                //get current date to integers
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1;
                var yyyy = today.getFullYear();
                //get current date string values
                var splitToday = String(today).split(" ");

                /*alert(splitToday[0] +">"+ splitOld[0])
                alert(splitToday[1] +">"+ splitOld[1])
                alert(splitToday[2] +">"+ splitOld[2])

                alert(yyyy +">"+ parseInt(split[0]))
                alert(mm +">"+ parseInt(split[1]))
                alert(dd +">"+ parseInt(split[2]) + dayOffset)*/

                //check if current day is saturday or sunday
                if( splitToday[0] === "Sun" || splitToday[0] === "Sat"){
                        objectName.oldData = splitOld[0] != "Fri";
                }
                else{
                        //check if cache is a year or more behind
                        if( yyyy > parseInt(split[0]) )
                                objectName.oldData = true;
                        //check if cache is a month or more behind
                        else{

                                if( mm > parseInt(split[1]) )
                                        objectName.oldData = true;
                                //check if cache is a day or more behind
                                else{
                                        objectName.oldData = dd > (parseInt(split[2]) + dayOffset );
                                }
                        }
                }

                //alert("end alert:" + objectName.oldData)
        },

};

/* * * * * * * * * * * * * *
 *                         *
 *       Graph Data        *
 *                         *
 * * * * * * * * * * * * * */
var gd = {

        'coinChart':{},

        drawGraph :function(){
        //while(platinumData == null || goldData == null || silverData == null);
        var pointStroke = "rgba(255,255,255,0.6)";
        var pointHighlightFill = "grey";
        var pointHighlightStroke = "#fff";

            var data = {
                    labels: gd.getLabels(goldAjax.storedData),
                    datasets: [
                    {
                            label: "Gold Total",
                            fillColor: "rgba(104, 206, 222, 0.05)",
                            strokeColor: "#FF6D67",
                            pointColor: "#FF6D67",
                            pointStrokeColor: pointStroke,
                            pointHighlightFill: pointHighlightFill,
                            pointHighlightStroke: pointHighlightStroke,
                            data: gd.getPrice(goldAjax.storedData)
                    },
                    {
                            label: "Platinum Total",
                            fillColor: "rgba(104, 206, 222, 0.05)",
                            strokeColor: "#FFA859",
                            pointColor: "#FFA859",
                            pointStrokeColor: pointStroke,
                            pointHighlightFill: pointHighlightFill,
                            pointHighlightStroke: pointHighlightStroke,
                            data: gd.getPrice(platinumAjax.storedData)
                    },
                    {
                            label: "Silver Total",
                            fillColor: "rgba(104, 206, 222, 0.05)",
                            strokeColor: "#F3FF88",
                            pointColor: "#F3FF88",
                            pointStrokeColor: pointStroke,
                            pointHighlightFill: pointHighlightFill,
                            pointHighlightStroke: pointHighlightStroke,
                            data: gd.getPrice(silverAjax.storedData)
                    },
                    ]
            };

                var options = {

            ///Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines : true,

            //String - Colour of the grid lines
            scaleGridLineColor : "rgba(104, 206, 222, 0.1)",

            //Number - Width of the grid lines
            scaleGridLineWidth : 1,

            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,

            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,

            //Boolean - Whether the line is curved between points
            bezierCurve : true,

            //Number - Tension of the bezier curve between points
            bezierCurveTension : 0.4,

            //Boolean - Whether to show a dot for each point
            pointDot : true,

            //Number - Radius of each point dot in pixels
            pointDotRadius : 4,

            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth : 1,

            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius : 20,

            //Boolean - Whether to show a stroke for datasets
            datasetStroke : true,

            //Number - Pixel width of dataset stroke
            datasetStrokeWidth : 2,

            //Boolean - Whether to fill the dataset with a colour
            datasetFill : true,

            //String - A legend template
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",

            responsive: true,

            maintainAspectRatio: false,

            //scale options
            scaleOverride: true, scaleStartValue: 0, scaleStepWidth: 80,scaleSteps: 20
            //mychart.Line(data,{scaleOverride: true, scaleStartValue: 0, scaleStepWidth: 1, scaleSteps: 30});

        };
            document.getElementById("total-chart").style.visibility = "visible";
            var ctx = document.getElementById("total-chart").getContext("2d");
            gd.coinChart = new Chart(ctx).Line(data,options);
            gd.coinChart.update();
        },


        drawSingleGraph : function(cp){
        //while(platinumData == null || goldData == null || silverData == null);
        var pointStroke = "rgba(255,255,255,0.6)";
        var pointHighlightFill = "grey";
        var pointHighlightStroke = "#fff";

        var data = {
            labels: gd.getLabels(cp),
            datasets: [
            {
                    label: "Gold Total",
                    fillColor: "rgba(104, 206, 222, 0.05)",
                    strokeColor: "#FF6D67",
                    pointColor: "#FF6D67",
                    pointStrokeColor: pointStroke,
                    pointHighlightFill: pointHighlightFill,
                    pointHighlightStroke: pointHighlightStroke,
                    data: gd.getPrice(cp)
            },
            /*{
                    label: "1oz Gold",
                    fillColor: "rgba(104, 206, 222, 0.05)",
                    strokeColor: "#9FFF98",
                    pointColor: "#9FFF98",
                    pointStrokeColor: pointStroke,
                    pointHighlightFill: pointHighlightFill,
                    pointHighlightStroke: pointHighlightStroke,
                    data: [100, 110, 120, 90, 102, 135, 115]
            }*/
            ]
        };

        var options = {

            ///Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines : true,

            //String - Colour of the grid lines
            scaleGridLineColor : "rgba(104, 206, 222, 0.1)",

            //Number - Width of the grid lines
            scaleGridLineWidth : 1,

            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,

            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,

            //Boolean - Whether the line is curved between points
            bezierCurve : true,

            //Number - Tension of the bezier curve between points
            bezierCurveTension : 0.4,

            //Boolean - Whether to show a dot for each point
            pointDot : true,

            //Number - Radius of each point dot in pixels
            pointDotRadius : 4,

            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth : 1,

            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius : 20,

            //Boolean - Whether to show a stroke for datasets
            datasetStroke : true,

            //Number - Pixel width of dataset stroke
            datasetStrokeWidth : 2,

            //Boolean - Whether to fill the dataset with a colour
            datasetFill : true,

            //String - A legend template
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",

            responsive: true,

            maintainAspectRatio: false,


        };
            document.getElementById("total-chart").style.visibility = "visible";
            var ctx = document.getElementById("total-chart").getContext("2d");
            gd.coinChart = new Chart(ctx).Line(data,options);
            gd.coinChart.update();
        },



        getPrice: function(xData){
            var prices = [];
            var j = 0;
            //1 denotes the gold price, 0 would be the date
            for( var i = xData.length - 1 ; i >= 0 ; --i){
                    prices[j] = xData[i][1];
                    j++;
            }

            return prices;

        },

        getLabels: function(xData){
            var labels = [];
            var tmpString;
            var split;
            var intString;
            var j = 0;
            //1 denotes the gold price, 0 would be the date
            for( var i = xData.length - 1; i >= 0 ; --i){
                    tmpString = xData[i][0];
                    split = tmpString.split("-");
                    labels[j] = gd.dateConversion(split);
                     j++;
            }

            return labels;

        },

        dateConversion: function(stringArray){

            var date = new Date(parseInt(stringArray[0]),
                    parseInt(stringArray[1])-1,parseInt(stringArray[2]));
            var splitAgain = String(date).split(" ");
            return splitAgain[0] + " " + splitAgain[1] + " " + splitAgain[2];
        },

        getAndSetDailyChangeSingle: function(xData,type){

            var dailyValueChange = parseInt(xData[0][1]) - parseInt(xData[1][1]);

            if( dailyValueChange >= 0 ){
                $("#daily-change" + "-" + type).addClass('pos-change');
                document.getElementById("daily-change"+ "-" + type).innerHTML = "+" + dailyValueChange;
            }
            else{
                $("#daily-change" + "-"+ type).addClass('neg-change');
                //dont have to add - sign to negative number, reddundant
                document.getElementById("daily-change"+ "-" + type).innerHTML =  dailyValueChange;
            }

        },

        setDailyValue: function(xData,type){

            document.getElementById(type + "AskingPrice").innerHTML = xData;
        },


};

/* * * * * * * * * * * * * *
 *                         *
 *        Gold Data        *
 *                         *
 * * * * * * * * * * * * * */
var goldAjax = {

        "nameString": "gold",

        'storedData': {},

        'oldData': true,

        currentPrice: function () {

                //maybe based on time of day get the AM or PM results
                return $.ajax({
                type: "GET",
                        //url: 'http://www.quandl.com/api/v1/datasets/WGC/GOLD_DAILY_USD.json?auth_token=UuKRQh3Hrxr-B_-yj6KB&rows=7&?trim_start=2012-11-01&trim_end=2012-11-30',
                        url: 'http://www.quandl.com/api/v1/datasets/LBMA/GOLD.json?auth_token=UuKRQh3Hrxr-B_-yj6KB&collapse=daily&rows=30',
                        dataType: "json",
                success: function(data){
                    goldAjax.storedData = data.data;
                    if( lc.bCacheSupported )
                                localStorage.setItem("goldAjaxData", JSON.stringify(goldAjax.storedData));
                },
                error: function(Xhr, textStatus, errorThrown) {
                        //jquery throwing error 0 on leave during
                        if(Xhr.status == 0 || Xhr.readyState == 0)
                                return;
                    alert("Status1: " + textStatus); alert("Error1: " + errorThrown);
                }
            });

        },

        getAllData: function (){

            $.when(goldAjax.currentPrice(), silverAjax.currentPrice(), platinumAjax.currentPrice()).done(function(a1, a2, a3){
                // the code here will be executed when all four ajax requests resolve.
                // a1, a2, a3 and a4 are lists of length 3 containing the response text,
                // status, and jqXHR object for each of the four ajax calls respectively.
                goldAjax.writeAllGraphData();
            });

        },

        writeAllGraphData: function() {
            gd.setDailyValue(goldAjax.storedData[0][1], goldAjax.nameString);
            gd.setDailyValue(silverAjax.storedData[0][1], silverAjax.nameString);
            gd.setDailyValue(platinumAjax.storedData[0][1], platinumAjax.nameString);
            gd.getAndSetDailyChangeSingle(goldAjax.storedData, goldAjax.nameString);
            gd.getAndSetDailyChangeSingle(silverAjax.storedData, silverAjax.nameString);
            gd.getAndSetDailyChangeSingle(platinumAjax.storedData, platinumAjax.nameString);
            document.getElementById('chart-is-loading').style.visibility = 'hidden';
            gd.drawGraph();
        },

        getData: function (){

            $.when(goldAjax.currentPrice()).done(function(a1){
                // the code here will be executed when all four ajax requests resolve.
                // a1, a2, a3 and a4 are lists of length 3 containing the response text,
                // status, and jqXHR object for each of the four ajax calls respectively.
                goldAjax.writeGraphData();
            });

        },

        writeGraphData: function() {
            gd.setDailyValue(goldAjax.storedData[0][1], goldAjax.nameString);
            gd.getAndSetDailyChangeSingle(goldAjax.storedData, goldAjax.nameString);
            document.getElementById('chart-is-loading').style.visibility = 'hidden';
            gd.drawSingleGraph(goldAjax.storedData);
        },

        writeGraphDynamic: function() {
            gd.coinChart.destroy();
            $(".current-legend").removeClass('current-legend');
            $("#legend-1").parent().addClass('current-legend');
            gd.drawSingleGraph(goldAjax.storedData);
        },

        writeAllGraphDynamic: function() {
            gd.coinChart.destroy();
            $(".current-legend").removeClass('current-legend');
            $("#legend-0").parent().addClass('current-legend');
            gd.drawGraph();
        },

};


/* * * * * * * * * * * * * *
 *                         *
 *        Silver Data      *
 *                         *
 * * * * * * * * * * * * * */
var silverAjax = {

        "nameString": "silver",

        'storedData': {},

        'oldData': true,

        currentPrice: function () {

            return $.ajax({
                type: "GET",
                        url: 'http://www.quandl.com/api/v1/datasets/LBMA/SILVER.json?auth_token=UuKRQh3Hrxr-B_-yj6KB&collapse=daily&rows=30',
                        dataType: "json",
                success: function(data){
                    silverAjax.storedData = data.data;
                    if( lc.bCacheSupported )
                                localStorage.setItem("silverAjaxData", JSON.stringify(silverAjax.storedData));
                },
                error: function(Xhr, textStatus, errorThrown) {
                        //jquery throwing error 0 on leave during
                        if(Xhr.status == 0 || Xhr.readyState == 0)
                                return;
                    alert("Status2: " + textStatus); alert("Error2: " + errorThrown);
                }
        });

        },

        getData: function (){

            $.when(silverAjax.currentPrice()).done(function(a2){
                // the code here will be executed when all four ajax requests resolve.
                // a1, a2, a3 and a4 are lists of length 3 containing the response text,
                // status, and jqXHR object for each of the four ajax calls respectively.
                silverAjax.writeGraphData();
            });

        },

        writeGraphData: function(){
            gd.setDailyValue(silverAjax.storedData[0][1], silverAjax.nameString);
            gd.getAndSetDailyChangeSingle(silverAjax.storedData, silverAjax.nameString);
            document.getElementById('chart-is-loading').style.visibility = 'hidden';
            gd.drawSingleGraph(silverAjax.storedData);
        },

        writeGraphDynamic: function() {
            gd.coinChart.destroy();
            $(".current-legend").removeClass('current-legend');
            $("#legend-3").parent().addClass('current-legend');
            gd.drawSingleGraph(silverAjax.storedData);
        },

};

/* * * * * * * * * * * * * *
 *                         *
 *    Platinum Data        *
 *                         *
 * * * * * * * * * * * * * */
var platinumAjax = {

        "nameString": "platinum",

        'storedData': {},

        'oldData': true,

        currentPrice: function() {

            return $.ajax({
                type: "GET",
                        url: 'https://www.quandl.com/api/v1/datasets/LPPM/PLAT.json?auth_token=UuKRQh3Hrxr-B_-yj6KB&collapse=daily&rows=30',
                        dataType: "json",
                success: function(data){
                    platinumAjax.storedData = data.data;
                    if( lc.bCacheSupported )
                                        localStorage.setItem("platinumAjaxData", JSON.stringify(platinumAjax.storedData));
                },
                error: function(Xhr, textStatus, errorThrown) {
                        //jquery throwing error 0 on leave during
                        if(Xhr.status == 0 || Xhr.readyState == 0)
                                return;
                    alert("Status3: " + textStatus); alert("Error3: " + errorThrown);
                }
            });

        },

        getData: function (){

            $.when(platinumAjax.currentPrice()).done(function(a3){
                // the code here will be executed when all four ajax requests resolve.
                // a1, a2, a3 and a4 are lists of length 3 containing the response text,
                // status, and jqXHR object for each of the four ajax calls respectively.
                platinumAjax.writeGraphData();
            });

        },

        writeGraphData: function(){
            gd.setDailyValue(platinumAjax.storedData[0][1], platinumAjax.nameString);
            gd.getAndSetDailyChangeSingle(platinumAjax.storedData,platinumAjax.nameString);
            document.getElementById('chart-is-loading').style.visibility = 'hidden';
            gd.drawSingleGraph(platinumAjax.storedData);
        },

        writeGraphDynamic: function() {
            gd.coinChart.destroy();
            $(".current-legend").removeClass('current-legend');
            $("#legend-2").parent().addClass('current-legend');
            gd.drawSingleGraph(platinumAjax.storedData);
        },

};


$(window).load(function() {
        var path = window.location.pathname;
        var page = path.split("/").pop();

        /* * * * * * * * * * * * * *
         *                         *
         *        GENERAL          *
         *                         *
         * * * * * * * * * * * * * */
        $("#settings-logout").click(function(){

            Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
            Parse.User.logOut();
            location.href = "./index.html";
        });
        $(".icon-cog").click(function(){
            location.href = "./settings.html";
        });

         $('tr').click(function(){
                $(this).find('a')[0].click();
         });

        /* * * * * * * * * * * * * *
         *                         *
         *        GRAPHING         *
         *                         *
         * * * * * * * * * * * * * */

        //get cached elements
        var goldC;
        var silverC;
        var platC;
        lc.bCacheSupported = lc.supports_html5_storage();

        //if browser supports local cache
        if( lc.bCacheSupported ){
            goldC = JSON.parse(localStorage.getItem("goldAjaxData"));
            silverC = JSON.parse(localStorage.getItem("silverAjaxData"));
            platC = JSON.parse(localStorage.getItem("platinumAjaxData"));
        }

        if(page == "home.html") {
                //display gif
                document.getElementById('chart-is-loading').style.visibility = 'visible';
                if( lc.bCacheSupported ){
                        if( goldC != null && silverC != null && platC != null ){
                            //check cache
                            lc.oldDataUpdate(goldC,0,goldAjax);
                            lc.oldDataUpdate(silverC,0,silverAjax);
                            lc.oldDataUpdate(platC,1,platinumAjax);

                            if( goldAjax.oldData )
                                    goldAjax.currentPrice();
                            else
                                    goldAjax.storedData = goldC;
                            if( silverAjax.oldData )
                                    silverAjax.currentPrice();
                            else
                                    silverAjax.storedData = silverC;
                            if( platinumAjax.oldData )
                                    platinumAjax.currentPrice();
                            else
                                    platinumAjax.storedData = platC;

                            goldAjax.writeAllGraphData();
                        }
                        else {
                            goldAjax.getAllData();
                        }
                }
                else {
                    goldAjax.getAllData();
                }

        }
        else if(page == "goldHome.html"){
                //display gif
                document.getElementById('chart-is-loading').style.visibility = 'visible';
                if( lc.bCacheSupported ){
                    //returns null if doesnt exist
                    if( goldC != null ){
                        lc.oldDataUpdate(goldC,0,goldAjax);
                        if( goldAjax.oldData )
                                goldAjax.currentPrice();
                        else
                                goldAjax.storedData = goldC;
                        //goldAjax.writeGraphData();
                    }
                    else{
                        goldAjax.getData();
                        //get other requests after graph/page load
                        if( silverC == null )
                                silverAjax.currentPrice();
                        if( platC == null )
                                platinumAjax.currentPrice();
                    }
                }
                else{
                    //goldAjax.getData();
                    goldAjax.currentPrice();
                }
                gd.setDailyValue(goldAjax.storedData[0][1], goldAjax.nameString);
                gd.getAndSetDailyChangeSingle(goldAjax.storedData, goldAjax.nameString);

        }
        else if(page == "silverHome.html"){

                //display gif
                document.getElementById('chart-is-loading').style.visibility = 'visible';

                if( lc.bCacheSupported ) {
                    //returns null if doesnt exist
                    if( silverC != null ){
                        lc.oldDataUpdate(silverC,0,silverAjax);
                        if( silverAjax.oldData )
                                silverAjax.currentPrice();
                        else
                                silverAjax.storedData = silverC;
                        //silverAjax.writeGraphData();
                    }
                    else{
                        silverAjax.getData();
                        //get other requests after graph/page load
                        //if( goldC == null )
                                //goldAjax.currentPrice();
                        if( platC == null )
                                platinumAjax.currentPrice();
                    }
                }
                else{
                    //silverAjax.getData();
                    silverAjax.currentPrice();
                }
                gd.setDailyValue(silverAjax.storedData[0][1], silverAjax.nameString);
                gd.getAndSetDailyChangeSingle(silverAjax.storedData, silverAjax.nameString);
        }
        else if(page == "platinumHome.html"){
                //display gif
                document.getElementById('chart-is-loading').style.visibility = 'visible';

                if( lc.bCacheSupported ){
                    //returns null if doesnt exist
                    if( platC != null ){
                        lc.oldDataUpdate(platC,1,platinumAjax);
                        if( platinumAjax.oldData )
                                platinumAjax.currentPrice();
                        else
                                platinumAjax.storedData = platC;
                        //platinumAjax.writeGraphData();
                    }
                    else{
                        platinumAjax.getData();
                        //get other requests after graph/page load
                        if( goldC == null )
                                goldAjax.currentPrice();
                        if( silverC == null )
                                silverAjax.currentPrice();
                    }
                }
                else{
                    //platinumAjax.getData();
                    platinumAjax.currentPrice();
                }
                gd.setDailyValue(platinumAjax.storedData[0][1], platinumAjax.nameString);
                gd.getAndSetDailyChangeSingle(platinumAjax.storedData,platinumAjax.nameString);

        }


        /* * * * * * * * * * * * * *
         *                         *
         *     MOBILE HANDLING     *
         *                         *
         * * * * * * * * * * * * * */

         $('.mtb-1').click(function(){
            $('.graph-panel').removeClass('graph-panel-show');
            $('.market-status').fadeIn(0);
            $('.market-list').fadeIn(0);
            if( page == "goldHome.html" || page == "silverHome.html" || page == "platinumHome.html" )
                    $('.my_stack').fadeIn(0);
            $('.mtb-2').removeClass('mobile-toggle-selected');
            $('.mtb-1').addClass('mobile-toggle-selected');

         });

         $('.mtb-2').click(function(){
            $('.market-status').fadeOut(0);
            $('.market-list').fadeOut(0);
            if( page == "goldHome.html" || page == "silverHome.html" || page == "platinumHome.html" )
                    $('.my_stack').fadeOut(0);
            $('.mtb-1').removeClass('mobile-toggle-selected');
            $('.mtb-2').addClass('mobile-toggle-selected');
            $('.graph-panel').addClass('graph-panel-show');
         });

         var resizer = function(){
                winWidth = $(window).width();
                winHeight = $(window).height();

                if (winWidth > 999){
                    $('.graph-panel').removeClass('graph-panel-show');
                    $('.market-status').fadeIn(0);
                    $('.market-list').fadeIn(0);
                    if( page == "goldHome.html" || page == "silverHome.html" || page == "platinumHome.html" )
                            $('.my_stack').fadeIn(0);
                    $('.mtb-2').removeClass('mobile-toggle-selected');
                    $('.mtb-1').addClass('mobile-toggle-selected');
                }
         };

         $(window).resize(resizer);


}); //end window on resize
