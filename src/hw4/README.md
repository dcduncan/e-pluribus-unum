
How We Built Our App:
We chose to use Parse as our backend webstore, and Quandl to pull all of our information. All of the information seen on every page is pulled when first run; then later, graphs and pictures become cached. More information in the Info For Graders section

Validations:
HTML : All good
CSS : All good from what we’ve added

Browser Compatability:
Chrome: 
All is good
Firefox: 
Facebook doesn’t necessarily support Firefox login through Facebook
Logout function isn’t recognized with parse
Opera:
‘loading…’ animation not supported
Safari:
‘loading…’ animation not supported
Internet Explorer:
If you try to use the website on a non-local environment, it will not work because Internet Explore has strict rules about SSL connections. The Parse JavaScript SDK uses SSL through ‘https’ to communicate with Parse, and Internet Explorer does not allow JavaScript calls to use SSL if the host is not using SSL itself. Source: https://www.parse.com/questions/how-does-parsecom-avoid-same-origin-policy-issues
Facebook login button does not login, it just leaves you hanging. Anything related to the parse JS library does not seem to work . Meaning that post and get requests do not work on any page and therefore it does not display any correct information. Therefore, you have to login through the other login system installed to get it to work correctly.  
In IE, the drop down in addCoin.html looks different then the other browsers (It has a white drop down arrow box). This is also the case with the ‘browse’ button in addCoin.html (white/gray shaded box).
Another browser issue is that, in IE, the input form tags on addCoin.html are different than the other browsers. When you try to increase the size of Qty., Premium, and Unit Price, it only lets you cancel that number or type it in manually. As opposed to the other browsers that lets you increase and decrease using the arrow buttons or clicking the increase portion of the input with your mouse. 

Other useful Info for Graders:
    
Due to Facebook login you will need to run our app at www.nardecky.com/Numismatics/e-pluribus-unum/src/hw4/index.html or run it locally out a localhost:63343
You may use either login method, there is a pre-populated account at                      email : test@test.com, password: password. At this time, you can log in with Facebook or create an account to see your bullion. If you want to test ‘logging out -> logging back in -> seeing if your coins are still there’, you must log out of Facebook directly from facebook.com if you logged in with facebook, then come back to the app and log in again from index.html, or hit the cog button in the top right. We plan to address the logout feature in assignment 5.
Feel free to add new bullion by clicking the plus icon, also editing and deleting existing bullion is possible by clicking on the coin row in the table and then clicking the very intuitive pencil icon beneath the picture.
There may be a long wait up front, but the app only needs that one load to maintain all its data for the site. It uses local storage to cache all the contents and, if local storage is not supported, it will just refresh every time. Data is also checked to see if it’s old and when it needs to be updated with a new request. You can leave page mid request, not suggested on the first page but each subsequent page loads all the data as well. It loads the data it needs to display the current graph (say gold graph) then once it’s on the screen, it gets data for silver and platinum.
Hitting ‘reload’ reloads the data (not the page) but Parse also ends the current session for some reason. So the custom reload button just clears the cache and gets a new request. This will stop you from adding coins and seeing personable data, obviously, but you can still use general features of seeing current data. 
Minified and concatenated main.js and parseHelper.js with grunt in dist folder
Css was minified with grunt as well, but some features like the phot wouldn't show up in dist folder

