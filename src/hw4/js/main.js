function loadFooter(){
	document.write("    <footer>");
	document.write("        &copy; 2015 Numismatics");
	document.write("    <\/footer> ");
};

/**
 * Returns true if a valid email
 * @param email
 */
function validateEmail(email) {
    var errors = {};
    var emailRegex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if (email.length == 0) {
        errors.errorMessage = "Please enter an email."
        errors.hasMessage = true;
    } else if (email.indexOf("@") < 0) {
        errors.errorMessage = "Email needs to contain an '@.'";
        errors.hasMessage = true;
    } else if (!emailRegex.test(email)) {
        errors.errorMessage = "Please enter a valid email format.";
        errors.hasMessage = true;
    }

    if (errors.hasMessage) {
        document.getElementById("user_email").style.background = '#AA0000';
        alert(errors.errorMessage);
    } else {
        document.getElementById("user_email").style.background = '#4c4c4c';
    }

    return !errors.hasMessage;
}

/**
 * Returns true if valid.
 * @param password
 * @param passwordConfirm
 * @returns {boolean}
 */
function validateChangePassword(password, passwordConfirm) {
    var errors = {};
    if (password !== passwordConfirm) {
        errors.errorMessage = "Passwords must match.";
        errors.hasError = true;
    } else if (password.length < 8) {
        errors.errorMessage = "Password must be atleast 8 characters.";
        errors.hasError = true;
    }

    if (errors.hasError) {
        document.getElementById("user_pCheck").style.background = '#AA0000';
        alert(errors.errorMessage);
    } else {
        document.getElementById("user_pCheck").style.background = '#4c4c4c';
    }

    return !errors.hasError;
}

function calculateMarket() {
    ms = {};
    ms.today = new Date();
    ms.date = ms.today.toString().split(" ");
    ms.date[4] = ms.date[4].split(":");
    ms.currentHours = parseInt(ms.date[4][0]);
    ms.currentMinutes = parseInt(ms.date[4][1]);
    ms.currentTime = parseInt(ms.date[4][0] + ms.date[4][1]);
    ms.timeRemaining = 0;
    ms.timeRemainingHours = 0;
    ms.timeRemainingMinutes = 0;
    ms.dayOfWeek = ms.date[0];
    ms.isMarketClosed = ms.currentTime < 630 || ms.currentTime > 1830 || ms.dayOfWeek == "Sun" || ms.dayOfWeek == "Sat";

    if (ms.isMarketClosed) {
        document.getElementById("marketOpenClosed").innerHTML = "Market is Closed";
        if (ms.currentTime < 630) {
            ms.timeRemainingHours = 6 - ms.currentHours;
            ms.timeRemainingMinutes = 30 - ms.currentMinutes;
        } else {
            ms.timeRemaining = 0;
            if (ms.currentTime > 1830) {
                ms.timeRemainingHours = 23 - ms.currentHours;
                ms.timeRemainingMinutes = 59 - ms.currentMinutes;
                ms.timeRemainingHours += 6;
                ms.timeRemainingMinutes += 30;
            }
        }

        if (ms.dayOfWeek == "Sat" || ms.dayOfWeek == "Sun") {

            if (ms.currentTime > 630) {
                ms.timeRemainingHours = 24 - ms.currentHours;
                ms.timeRemainingMinutes = 30 - ms.currentMinutes;
                ms.timeRemainingHours += 6;
                ms.timeRemainingMinutes += 30;
            } else {
                ms.timeRemainingHours = 6 - ms.currentHours;
                ms.timeRemainingMinutes = 30 - ms.currentMinutes;
            }

            if (ms.dayOfWeek == "Sat") {
                ms.timeRemainingHours += 24;
            }

            if (ms.currentTime < 630) {
                ms.timeRemainingHours += 24;
            }

            ms.timeRemainingHours--;
            ms.timeRemainingMinutes += 30;
            if (ms.timeRemainingMinutes > 60) {
                ms.timeRemainingMinutes = ms.timeRemainingMinutes % 60;
                ++ms.timeRemainingHours;
            }
        }

        if (ms.timeRemainingMinutes < 0) {
            ms.timeRemaining *= -1;
        }

        document.getElementById("marketTime").innerHTML = "Opens in " + ms.timeRemainingHours + "h " + ms.timeRemainingMinutes + "min.";

    } else {
        document.getElementById("marketOpenClosed").innerHTML = "Market is Open";
        ms.timeRemainingHours = 18 - ms.currentHours;
        if (ms.currentMinutes > 30) {
            ms.timeRemainingMinutes = ms.timeRemainingMinutes - 30;
            ms.timeRemainingHours--;
        } else {
            ms.timeRemainingMinutes = 30 - ms.timeRemainingMinutes;
        }

        if (ms.timeRemainingMinutes < 0) {
            ms.timeRemaining *= -1;
        }

        document.getElementById("marketTime").innerHTML = "Closes in " + ms.timeRemainingHours + "h " + ms.timeRemainingMinutes + "min.";
    }
};
/* * * * * * * * * * * * * *
 *                         *
 *       Cache Data        *
 *                         *
 * * * * * * * * * * * * * */
var lc = {

        supports_html5_storage: function() {
          try {
            return 'localStorage' in window && window['localStorage'] !== null;
          } catch (e) {
            return false;
          }
        },

        'bCacheSupported': false,

        oldDataUpdate: function(xData, dayOffset, objectName){
                var cachedDate = xData[0][0];           //current cached date
                var split = cachedDate.split("-");      //split cached integers saved as strings
                //get a string format of our cached date
                var oldDay = new Date(parseInt(split[0]),
                        parseInt(split[1])-1,parseInt(split[2]));
                var splitOld = String(oldDay).split(" ");
                //get current date to integers
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1;
                var yyyy = today.getFullYear();
                //get current date string values
                var splitToday = String(today).split(" ");

                /*alert(splitToday[0] +">"+ splitOld[0])
                alert(splitToday[1] +">"+ splitOld[1])
                alert(splitToday[2] +">"+ splitOld[2])

                alert(yyyy +">"+ parseInt(split[0]))
                alert(mm +">"+ parseInt(split[1]))
                alert(dd +">"+ parseInt(split[2]) + dayOffset)*/

                //check if current day is saturday or sunday
                if( splitToday[0] === "Sun" || splitToday[0] === "Sat"){
                        objectName.oldData = splitOld[0] != "Fri";
                }
                else{
                        //check if cache is a year or more behind
                        if( yyyy > parseInt(split[0]) )
                                objectName.oldData = true;
                        //check if cache is a month or more behind
                        else{

                                if( mm > parseInt(split[1]) )
                                        objectName.oldData = true;
                                //check if cache is a day or more behind
                                else{
                                        objectName.oldData = dd > (parseInt(split[2]) + dayOffset );
                                }
                        }
                }

                //alert("end alert:" + objectName.oldData)
        },

};

/* * * * * * * * * * * * * *
 *                         *
 *       Graph Data        *
 *                         *
 * * * * * * * * * * * * * */
var gd = {

        'coinChart':{},

        drawGraph :function(){
        //while(platinumData == null || goldData == null || silverData == null);
        var pointStroke = "rgba(255,255,255,0.6)";
        var pointHighlightFill = "grey";
        var pointHighlightStroke = "#fff";

            var data = {
                    labels: gd.getLabels(goldAjax.storedData),
                    datasets: [
                    {
                            label: "Gold Total",
                            fillColor: "rgba(104, 206, 222, 0.05)",
                            strokeColor: "#FF6D67",
                            pointColor: "#FF6D67",
                            pointStrokeColor: pointStroke,
                            pointHighlightFill: pointHighlightFill,
                            pointHighlightStroke: pointHighlightStroke,
                            data: gd.getPrice(goldAjax.storedData)
                    },
                    {
                            label: "Platinum Total",
                            fillColor: "rgba(104, 206, 222, 0.05)",
                            strokeColor: "#FFA859",
                            pointColor: "#FFA859",
                            pointStrokeColor: pointStroke,
                            pointHighlightFill: pointHighlightFill,
                            pointHighlightStroke: pointHighlightStroke,
                            data: gd.getPrice(platinumAjax.storedData)
                    },
                    {
                            label: "Silver Total",
                            fillColor: "rgba(104, 206, 222, 0.05)",
                            strokeColor: "#F3FF88",
                            pointColor: "#F3FF88",
                            pointStrokeColor: pointStroke,
                            pointHighlightFill: pointHighlightFill,
                            pointHighlightStroke: pointHighlightStroke,
                            data: gd.getPrice(silverAjax.storedData)
                    },
                    ]
            };

                var options = {

            ///Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines : true,

            //String - Colour of the grid lines
            scaleGridLineColor : "rgba(104, 206, 222, 0.1)",

            //Number - Width of the grid lines
            scaleGridLineWidth : 1,

            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,

            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,

            //Boolean - Whether the line is curved between points
            bezierCurve : true,

            //Number - Tension of the bezier curve between points
            bezierCurveTension : 0.4,

            //Boolean - Whether to show a dot for each point
            pointDot : true,

            //Number - Radius of each point dot in pixels
            pointDotRadius : 4,

            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth : 1,

            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius : 20,

            //Boolean - Whether to show a stroke for datasets
            datasetStroke : true,

            //Number - Pixel width of dataset stroke
            datasetStrokeWidth : 2,

            //Boolean - Whether to fill the dataset with a colour
            datasetFill : true,

            //String - A legend template
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",

            responsive: true,

            maintainAspectRatio: false,

            //scale options
            scaleOverride: true, scaleStartValue: 0, scaleStepWidth: 80,scaleSteps: 20
            //mychart.Line(data,{scaleOverride: true, scaleStartValue: 0, scaleStepWidth: 1, scaleSteps: 30});

        };
            document.getElementById("total-chart").style.visibility = "visible";
            var ctx = document.getElementById("total-chart").getContext("2d");
            gd.coinChart = new Chart(ctx).Line(data,options);
            gd.coinChart.update();
        },


        drawSingleGraph : function(cp){
        //while(platinumData == null || goldData == null || silverData == null);
        var pointStroke = "rgba(255,255,255,0.6)";
        var pointHighlightFill = "grey";
        var pointHighlightStroke = "#fff";

        var data = {
            labels: gd.getLabels(cp),
            datasets: [
            {
                    label: "Gold Total",
                    fillColor: "rgba(104, 206, 222, 0.05)",
                    strokeColor: "#FF6D67",
                    pointColor: "#FF6D67",
                    pointStrokeColor: pointStroke,
                    pointHighlightFill: pointHighlightFill,
                    pointHighlightStroke: pointHighlightStroke,
                    data: gd.getPrice(cp)
            },
            /*{
                    label: "1oz Gold",
                    fillColor: "rgba(104, 206, 222, 0.05)",
                    strokeColor: "#9FFF98",
                    pointColor: "#9FFF98",
                    pointStrokeColor: pointStroke,
                    pointHighlightFill: pointHighlightFill,
                    pointHighlightStroke: pointHighlightStroke,
                    data: [100, 110, 120, 90, 102, 135, 115]
            }*/
            ]
        };

        var options = {

            ///Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines : true,

            //String - Colour of the grid lines
            scaleGridLineColor : "rgba(104, 206, 222, 0.1)",

            //Number - Width of the grid lines
            scaleGridLineWidth : 1,

            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,

            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,

            //Boolean - Whether the line is curved between points
            bezierCurve : true,

            //Number - Tension of the bezier curve between points
            bezierCurveTension : 0.4,

            //Boolean - Whether to show a dot for each point
            pointDot : true,

            //Number - Radius of each point dot in pixels
            pointDotRadius : 4,

            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth : 1,

            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius : 20,

            //Boolean - Whether to show a stroke for datasets
            datasetStroke : true,

            //Number - Pixel width of dataset stroke
            datasetStrokeWidth : 2,

            //Boolean - Whether to fill the dataset with a colour
            datasetFill : true,

            //String - A legend template
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",

            responsive: true,

            maintainAspectRatio: false,


        };
            document.getElementById("total-chart").style.visibility = "visible";
            var ctx = document.getElementById("total-chart").getContext("2d");
            gd.coinChart = new Chart(ctx).Line(data,options);
            gd.coinChart.update();
        },



        getPrice: function(xData){
            var prices = [];
            var j = 0;
            //1 denotes the gold price, 0 would be the date
            for( var i = xData.length - 1 ; i >= 0 ; --i){
                    prices[j] = xData[i][1];
                    j++;
            }

            return prices;

        },

        getLabels: function(xData){
            var labels = [];
            var tmpString;
            var split;
            var intString;
            var j = 0;
            //1 denotes the gold price, 0 would be the date
            for( var i = xData.length - 1; i >= 0 ; --i){
                    tmpString = xData[i][0];
                    split = tmpString.split("-");
                    labels[j] = gd.dateConversion(split);
                     j++;
            }

            return labels;

        },

        dateConversion: function(stringArray){

            var date = new Date(parseInt(stringArray[0]),
                    parseInt(stringArray[1])-1,parseInt(stringArray[2]));
            var splitAgain = String(date).split(" ");
            return splitAgain[0] + " " + splitAgain[1] + " " + splitAgain[2];
        },

        getAndSetDailyChangeSingle: function(xData,type){

            var dailyValueChange = parseInt(xData[0][1]) - parseInt(xData[1][1]);

            if( dailyValueChange >= 0 ){
                $("#daily-change" + "-" + type).addClass('pos-change');
                document.getElementById("daily-change"+ "-" + type).innerHTML = "+" + dailyValueChange;
            }
            else{
                $("#daily-change" + "-"+ type).addClass('neg-change');
                //dont have to add - sign to negative number, reddundant
                document.getElementById("daily-change"+ "-" + type).innerHTML =  dailyValueChange;
            }

        },

        setDailyValue: function(xData,type){

            document.getElementById(type + "AskingPrice").innerHTML = xData;
        },


};

/* * * * * * * * * * * * * *
 *                         *
 *        Gold Data        *
 *                         *
 * * * * * * * * * * * * * */
var goldAjax = {

        "nameString": "gold",

        'storedData': {},

        'oldData': true,

        currentPrice: function () {

                //maybe based on time of day get the AM or PM results
                return $.ajax({
                type: "GET",
                        //url: 'http://www.quandl.com/api/v1/datasets/WGC/GOLD_DAILY_USD.json?auth_token=UuKRQh3Hrxr-B_-yj6KB&rows=7&?trim_start=2012-11-01&trim_end=2012-11-30',
                        url: 'http://www.quandl.com/api/v1/datasets/LBMA/GOLD.json?auth_token=UuKRQh3Hrxr-B_-yj6KB&collapse=daily&rows=30',
                        dataType: "json",
                success: function(data){
                    goldAjax.storedData = data.data;
                    if( lc.bCacheSupported )
                                localStorage.setItem("goldAjaxData", JSON.stringify(goldAjax.storedData));
                },
                error: function(Xhr, textStatus, errorThrown) {
                        //jquery throwing error 0 on leave during
                        if(Xhr.status == 0 || Xhr.readyState == 0)
                                return;
                    alert("Status1: " + textStatus); alert("Error1: " + errorThrown);
                }
            });

        },

        getAllData: function (){

            $.when(goldAjax.currentPrice(), silverAjax.currentPrice(), platinumAjax.currentPrice()).done(function(a1, a2, a3){
                // the code here will be executed when all four ajax requests resolve.
                // a1, a2, a3 and a4 are lists of length 3 containing the response text,
                // status, and jqXHR object for each of the four ajax calls respectively.
                goldAjax.writeAllGraphData();
            });

        },

        writeAllGraphData: function() {
            gd.setDailyValue(goldAjax.storedData[0][1], goldAjax.nameString);
            gd.setDailyValue(silverAjax.storedData[0][1], silverAjax.nameString);
            gd.setDailyValue(platinumAjax.storedData[0][1], platinumAjax.nameString);
            gd.getAndSetDailyChangeSingle(goldAjax.storedData, goldAjax.nameString);
            gd.getAndSetDailyChangeSingle(silverAjax.storedData, silverAjax.nameString);
            gd.getAndSetDailyChangeSingle(platinumAjax.storedData, platinumAjax.nameString);
            document.getElementById('chart-is-loading').style.visibility = 'hidden';
            gd.drawGraph();
        },

        getData: function (){

            $.when(goldAjax.currentPrice()).done(function(a1){
                // the code here will be executed when all four ajax requests resolve.
                // a1, a2, a3 and a4 are lists of length 3 containing the response text,
                // status, and jqXHR object for each of the four ajax calls respectively.
                goldAjax.writeGraphData();
            });

        },

        writeGraphData: function() {
            gd.setDailyValue(goldAjax.storedData[0][1], goldAjax.nameString);
            gd.getAndSetDailyChangeSingle(goldAjax.storedData, goldAjax.nameString);
            document.getElementById('chart-is-loading').style.visibility = 'hidden';
            gd.drawSingleGraph(goldAjax.storedData);
        },

        writeGraphDynamic: function() {
            gd.coinChart.destroy();
            $(".current-legend").removeClass('current-legend');
            $("#legend-1").parent().addClass('current-legend');
            gd.drawSingleGraph(goldAjax.storedData);
        },

        writeAllGraphDynamic: function() {
            gd.coinChart.destroy();
            $(".current-legend").removeClass('current-legend');
            $("#legend-0").parent().addClass('current-legend');
            gd.drawGraph();
        },

};


/* * * * * * * * * * * * * *
 *                         *
 *        Silver Data      *
 *                         *
 * * * * * * * * * * * * * */
var silverAjax = {

        "nameString": "silver",

        'storedData': {},

        'oldData': true,

        currentPrice: function () {

            return $.ajax({
                type: "GET",
                        url: 'http://www.quandl.com/api/v1/datasets/LBMA/SILVER.json?auth_token=UuKRQh3Hrxr-B_-yj6KB&collapse=daily&rows=30',
                        dataType: "json",
                success: function(data){
                    silverAjax.storedData = data.data;
                    if( lc.bCacheSupported )
                                localStorage.setItem("silverAjaxData", JSON.stringify(silverAjax.storedData));
                },
                error: function(Xhr, textStatus, errorThrown) {
                        //jquery throwing error 0 on leave during
                        if(Xhr.status == 0 || Xhr.readyState == 0)
                                return;
                    alert("Status2: " + textStatus); alert("Error2: " + errorThrown);
                }
        });

        },

        getData: function (){

            $.when(silverAjax.currentPrice()).done(function(a2){
                // the code here will be executed when all four ajax requests resolve.
                // a1, a2, a3 and a4 are lists of length 3 containing the response text,
                // status, and jqXHR object for each of the four ajax calls respectively.
                silverAjax.writeGraphData();
            });

        },

        writeGraphData: function(){
            gd.setDailyValue(silverAjax.storedData[0][1], silverAjax.nameString);
            gd.getAndSetDailyChangeSingle(silverAjax.storedData, silverAjax.nameString);
            document.getElementById('chart-is-loading').style.visibility = 'hidden';
            gd.drawSingleGraph(silverAjax.storedData);
        },

        writeGraphDynamic: function() {
            gd.coinChart.destroy();
            $(".current-legend").removeClass('current-legend');
            $("#legend-3").parent().addClass('current-legend');
            gd.drawSingleGraph(silverAjax.storedData);
        },

};

/* * * * * * * * * * * * * *
 *                         *
 *    Platinum Data        *
 *                         *
 * * * * * * * * * * * * * */
var platinumAjax = {

        "nameString": "platinum",

        'storedData': {},

        'oldData': true,

        currentPrice: function() {

            return $.ajax({
                type: "GET",
                        url: 'https://www.quandl.com/api/v1/datasets/LPPM/PLAT.json?auth_token=UuKRQh3Hrxr-B_-yj6KB&collapse=daily&rows=30',
                        dataType: "json",
                success: function(data){
                    platinumAjax.storedData = data.data;
                    if( lc.bCacheSupported )
                                        localStorage.setItem("platinumAjaxData", JSON.stringify(platinumAjax.storedData));
                },
                error: function(Xhr, textStatus, errorThrown) {
                        //jquery throwing error 0 on leave during
                        if(Xhr.status == 0 || Xhr.readyState == 0)
                                return;
                    alert("Status3: " + textStatus); alert("Error3: " + errorThrown);
                }
            });

        },

        getData: function (){

            $.when(platinumAjax.currentPrice()).done(function(a3){
                // the code here will be executed when all four ajax requests resolve.
                // a1, a2, a3 and a4 are lists of length 3 containing the response text,
                // status, and jqXHR object for each of the four ajax calls respectively.
                platinumAjax.writeGraphData();
            });

        },

        writeGraphData: function(){
            gd.setDailyValue(platinumAjax.storedData[0][1], platinumAjax.nameString);
            gd.getAndSetDailyChangeSingle(platinumAjax.storedData,platinumAjax.nameString);
            document.getElementById('chart-is-loading').style.visibility = 'hidden';
            gd.drawSingleGraph(platinumAjax.storedData);
        },

        writeGraphDynamic: function() {
            gd.coinChart.destroy();
            $(".current-legend").removeClass('current-legend');
            $("#legend-2").parent().addClass('current-legend');
            gd.drawSingleGraph(platinumAjax.storedData);
        },

};


$(window).load(function() {
        var path = window.location.pathname;
        var page = path.split("/").pop();

        /* * * * * * * * * * * * * *
         *                         *
         *        GENERAL          *
         *                         *
         * * * * * * * * * * * * * */
        $("#settings-logout").click(function(){

            Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
            Parse.User.logOut();
            location.href = "./index.html";
        });
        $(".icon-cog").click(function(){
            location.href = "./settings.html";
        });

         $('tr').click(function(){
                $(this).find('a')[0].click();
         });

        /* * * * * * * * * * * * * *
         *                         *
         *        GRAPHING         *
         *                         *
         * * * * * * * * * * * * * */

        //get cached elements
        var goldC;
        var silverC;
        var platC;
        lc.bCacheSupported = lc.supports_html5_storage();

        //if browser supports local cache
        if( lc.bCacheSupported ){
            goldC = JSON.parse(localStorage.getItem("goldAjaxData"));
            silverC = JSON.parse(localStorage.getItem("silverAjaxData"));
            platC = JSON.parse(localStorage.getItem("platinumAjaxData"));
        }

        if(page == "home.html") {
                //display gif
                document.getElementById('chart-is-loading').style.visibility = 'visible';
                if( lc.bCacheSupported ){
                        if( goldC != null && silverC != null && platC != null ){
                            //check cache
                            lc.oldDataUpdate(goldC,0,goldAjax);
                            lc.oldDataUpdate(silverC,0,silverAjax);
                            lc.oldDataUpdate(platC,1,platinumAjax);

                            if( goldAjax.oldData )
                                    goldAjax.currentPrice();
                            else
                                    goldAjax.storedData = goldC;
                            if( silverAjax.oldData )
                                    silverAjax.currentPrice();
                            else
                                    silverAjax.storedData = silverC;
                            if( platinumAjax.oldData )
                                    platinumAjax.currentPrice();
                            else
                                    platinumAjax.storedData = platC;

                            goldAjax.writeAllGraphData();
                        }
                        else {
                            goldAjax.getAllData();
                        }
                }
                else {
                    goldAjax.getAllData();
                }

        }
        else if(page == "goldHome.html"){
                //display gif
                document.getElementById('chart-is-loading').style.visibility = 'visible';
                if( lc.bCacheSupported ){
                    //returns null if doesnt exist
                    if( goldC != null ){
                        lc.oldDataUpdate(goldC,0,goldAjax);
                        if( goldAjax.oldData )
                                goldAjax.currentPrice();
                        else
                                goldAjax.storedData = goldC;
                        //goldAjax.writeGraphData();
                    }
                    else{
                        goldAjax.getData();
                        //get other requests after graph/page load
                        if( silverC == null )
                                silverAjax.currentPrice();
                        if( platC == null )
                                platinumAjax.currentPrice();
                    }
                }
                else{
                    //goldAjax.getData();
                    goldAjax.currentPrice();
                }
                gd.setDailyValue(goldAjax.storedData[0][1], goldAjax.nameString);
                gd.getAndSetDailyChangeSingle(goldAjax.storedData, goldAjax.nameString);

        }
        else if(page == "silverHome.html"){

                //display gif
                document.getElementById('chart-is-loading').style.visibility = 'visible';

                if( lc.bCacheSupported ) {
                    //returns null if doesnt exist
                    if( silverC != null ){
                        lc.oldDataUpdate(silverC,0,silverAjax);
                        if( silverAjax.oldData )
                                silverAjax.currentPrice();
                        else
                                silverAjax.storedData = silverC;
                        //silverAjax.writeGraphData();
                    }
                    else{
                        silverAjax.getData();
                        //get other requests after graph/page load
                        //if( goldC == null )
                                //goldAjax.currentPrice();
                        if( platC == null )
                                platinumAjax.currentPrice();
                    }
                }
                else{
                    //silverAjax.getData();
                    silverAjax.currentPrice();
                }
                gd.setDailyValue(silverAjax.storedData[0][1], silverAjax.nameString);
                gd.getAndSetDailyChangeSingle(silverAjax.storedData, silverAjax.nameString);
        }
        else if(page == "platinumHome.html"){
                //display gif
                document.getElementById('chart-is-loading').style.visibility = 'visible';

                if( lc.bCacheSupported ){
                    //returns null if doesnt exist
                    if( platC != null ){
                        lc.oldDataUpdate(platC,1,platinumAjax);
                        if( platinumAjax.oldData )
                                platinumAjax.currentPrice();
                        else
                                platinumAjax.storedData = platC;
                        //platinumAjax.writeGraphData();
                    }
                    else{
                        platinumAjax.getData();
                        //get other requests after graph/page load
                        if( goldC == null )
                                goldAjax.currentPrice();
                        if( silverC == null )
                                silverAjax.currentPrice();
                    }
                }
                else{
                    //platinumAjax.getData();
                    platinumAjax.currentPrice();
                }
                gd.setDailyValue(platinumAjax.storedData[0][1], platinumAjax.nameString);
                gd.getAndSetDailyChangeSingle(platinumAjax.storedData,platinumAjax.nameString);

        }


        /* * * * * * * * * * * * * *
         *                         *
         *     MOBILE HANDLING     *
         *                         *
         * * * * * * * * * * * * * */

         $('.mtb-1').click(function(){
            $('.graph-panel').removeClass('graph-panel-show');
            $('.market-status').fadeIn(0);
            $('.market-list').fadeIn(0);
            if( page == "goldHome.html" || page == "silverHome.html" || page == "platinumHome.html" )
                    $('.my_stack').fadeIn(0);
            $('.mtb-2').removeClass('mobile-toggle-selected');
            $('.mtb-1').addClass('mobile-toggle-selected');

         });

         $('.mtb-2').click(function(){
            $('.market-status').fadeOut(0);
            $('.market-list').fadeOut(0);
            if( page == "goldHome.html" || page == "silverHome.html" || page == "platinumHome.html" )
                    $('.my_stack').fadeOut(0);
            $('.mtb-1').removeClass('mobile-toggle-selected');
            $('.mtb-2').addClass('mobile-toggle-selected');
            $('.graph-panel').addClass('graph-panel-show');
         });

         var resizer = function(){
                winWidth = $(window).width();
                winHeight = $(window).height();

                if (winWidth > 999){
                    $('.graph-panel').removeClass('graph-panel-show');
                    $('.market-status').fadeIn(0);
                    $('.market-list').fadeIn(0);
                    if( page == "goldHome.html" || page == "silverHome.html" || page == "platinumHome.html" )
                            $('.my_stack').fadeIn(0);
                    $('.mtb-2').removeClass('mobile-toggle-selected');
                    $('.mtb-1').addClass('mobile-toggle-selected');
                }
         };

         $(window).resize(resizer);


}); //end window on resize
