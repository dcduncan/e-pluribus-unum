
//object.purchasedDate
//object.value
var userGraphInfo = {

    'storedData': {},

    getLabels: function(xData){
        var labels = [];
        var tmpString;
        var split;
        console.log("getLabels");
        console.log(xData);
        for( var i = 0; i < xData.length ; ++i){
                tmpString = String(xData[i].purchaseDate);
                split = tmpString.split(" ");
                labels[i] = split[0] + " " + split[1] + " " + split[2];
        }

        return labels;
    },

    getPrice: function(xData){
        var prices = [];
        console.log(xData);
        for( var i = 0; i < xData.length; ++i){
            if( i != 0)
                prices[i] = (xData[i].value ) +  prices[i-1];
            else
                prices[i] = (xData[i].value);
        }

        return prices;

    },

    drawUserInfoGraph : function(){
        //while(platinumData == null || goldData == null || silverData == null);
        var pointStroke = "rgba(255,255,255,0.6)";
        var pointHighlightFill = "grey";
        var pointHighlightStroke = "#fff";

        var data = {
            labels: userGraphInfo.getLabels(userGraphInfo.storedData),
            datasets: [
                {
                    label: "User Total",
                    fillColor: "rgba(104, 206, 222, 0.05)",
                    strokeColor: "#FF6D67",
                    pointColor: "#FF6D67",
                    pointStrokeColor: pointStroke,
                    pointHighlightFill: pointHighlightFill,
                    pointHighlightStroke: pointHighlightStroke,
                    data: userGraphInfo.getPrice(userGraphInfo.storedData)
                },
                /*{
                 label: "1oz Gold",
                 fillColor: "rgba(104, 206, 222, 0.05)",
                 strokeColor: "#9FFF98",
                 pointColor: "#9FFF98",
                 pointStrokeColor: pointStroke,
                 pointHighlightFill: pointHighlightFill,
                 pointHighlightStroke: pointHighlightStroke,
                 data: [100, 110, 120, 90, 102, 135, 115]
                 }*/
            ]
        };

        var options = {

            ///Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines : true,

            //String - Colour of the grid lines
            scaleGridLineColor : "rgba(104, 206, 222, 0.1)",

            //Number - Width of the grid lines
            scaleGridLineWidth : 1,

            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,

            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,

            //Boolean - Whether the line is curved between points
            bezierCurve : true,

            //Number - Tension of the bezier curve between points
            bezierCurveTension : 0.4,

            //Boolean - Whether to show a dot for each point
            pointDot : true,

            //Number - Radius of each point dot in pixels
            pointDotRadius : 4,

            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth : 1,

            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius : 20,

            //Boolean - Whether to show a stroke for datasets
            datasetStroke : true,

            //Number - Pixel width of dataset stroke
            datasetStrokeWidth : 2,

            //Boolean - Whether to fill the dataset with a colour
            datasetFill : true,

            //String - A legend template
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",

            responsive: true,

            maintainAspectRatio: false,


        };

        if (userGraphInfo.storedData.length === 0) {
            document.getElementById("poor-span").innerHTML = "YOU ARE TOO POOR FOR COINS!";
        } else {
            document.getElementById("total-chart").style.visibility = "visible";
            var ctx = document.getElementById("total-chart").getContext("2d");
            var coinChart = new Chart(ctx).Line(data, options);
            coinChart.update();
        }
        document.getElementById("chart-is-loading").style.visibility = "hidden";
    },
};


/**
 * Created by niklj_000 on 5/25/2015.
 */

/**
 * Returns the boolean if the user has a dark theme preference.
 * @returns {boolean}
 */
function getDarkThemePreference() {
    Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
    Parse.User.current().fetch().then(function (user) {
        if (user.get("darkTheme")) {
            var yourElement = document.getElementById('styleSheetCheck');
            yourElement.setAttribute('href', './style/style.css');
            //document.getElementById('chart-is-loading').src = "./img/ajax-loader.gif"
        }
        console.log("Updated.");
    });
}

/**
 * Sets the users dark theme preference
 * @param wantsDarkTheme
 */
function setDarkThemePreference(wantsDarkTheme) {
    Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
    Parse.User.current().fetch().then(function (user) {
        user.set("darkTheme", wantsDarkTheme);
        console.log(wantsDarkTheme);
        user.save();
    });
}

/**
 * Returns the boolean if the user has a light theme preference.
 * @returns {boolean}
 */
function getLightThemePreference() {
    return !getDarkThemePreference();
}

/**
 * Sets the users light theme preference
 * @param wantsLightTheme
 */
function setLightThemePreference(wantsLightTheme) {
    setDarkThemePreference(!wantsLightTheme);
}

/**
 * Sets the users  password to the provided password
 * @param password
 */
function setPassword(password) {
    Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
    Parse.User.current().fetch().then(function (user) {
        user.set("password", password);
        console.log(password);
        user.save();
        alert("Password updated.");
    });
}

/**
 * Sets the users email to the provided email
 * @param email
 */
function setEmail(email) {
    Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
    Parse.User.current().fetch().then(function (user) {
        user.set("email", email);
        user.set("username", email);
        console.log(email);
        user.save();
        alert("Email updated.");
    });
}

/**
 *
 * @param designator, The title of the content that would like to be retrieved ie: metal, type...
 * @param specifier, The actual value of the thing to compare with the designator
 * @param detailsToGrab, an array of Strings with all of the details to grab about each of the metals
 * @return an array of objects that contain all of the data to populate the table in the specific gold, silver and platinum pages.
 */
function getBullion(designator, specifier, detailsToGrab) {
    Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
    Parse.User.current().fetch().then(function (user) {
        var userName = user.get("username");
        var Bullion = Parse.Object.extend("Bullion");
        var query = new Parse.Query(Bullion);
        query.equalTo("userName", userName);
        query.equalTo(designator, specifier);
        query.ascending("purchaseDate");
        query.find({
            success: function(results) {
                formattedResults = [];
                for (var i = 0; i < results.length; ++i) {
                    var object = results[i];

                    formattedResults[i] = {};

                    for (var j = 0; j < detailsToGrab.length; ++j) {
                        if(j == 0 && object.get(detailsToGrab[j]) != null)
                            formattedResults[i][detailsToGrab[j]] = object.get(detailsToGrab[j]).url();
                        else
                            formattedResults[i][detailsToGrab[j]] = object.get(detailsToGrab[j]);
                    }
                }
                //get info for printing graph

                userGraphInfo.storedData = formattedResults;
                userGraphInfo.drawUserInfoGraph();
                populateBullionTable(formattedResults);
                return formattedResults;
            },
            error: function(error) {
                console.log("Error getting all bullion: " + error.code + " -> " + error.message);
                return undefined;
            }
        });
    });
}

/**
 * Retrieves all of the details about a specific bullion for the current user
 * @param type, The string of the type of bullion that you would like retrieved.
 * @return an array of objects that contain all of the data to populate the detail page for the specific bullion.
 */
function getBullionsForItemDetails(metal) {
    var detailsToGrab = ["picture", "metal", "type", "purchaseDate", "quantity", "premium", "unitPrice", "percent", "weight", "gu", "ozt", "totalOZT", "value"];
    return getBullion("metal", metal, detailsToGrab);
}

/**
 *
 * @param metal, The string of the type of metal that you would like retrieved; takes 3 values: gold, silver, and platinum for gold, silver and platinum respectively
 * @return an array of objects that contain all of the data to populate the table in the specific gold, silver and platinum pages.
 */
function getBullionForTable(metal) {
    var detailsToGrab = ["picture", "type", "quantity", "weight", "percent", "value"];
    return getBullion("metal", metal, detailsToGrab);
}


function populateBullionTable(bullionList) {
    var bullionTable = document.getElementById("bullionTableBody");
    var totalValue = 0;

    for (i = 0; i < bullionList.length; i++) {
        var tr = document.createElement('TR');
        tr.setAttribute("id", "coin"+i);
        tr.setAttribute("onclick", "getClickedElement("+i+");");


        var imgTD = document.createElement("TD");
        imgTD.className = "stack_img_col";

        var imgDiv = document.createElement("DIV");
        imgDiv.className = "coin_mini";

        imgTD.appendChild(imgDiv);

        var infoAnchor = document.createElement("A");
        infoAnchor.setAttribute("href", "coinInfo.html");

        var detailsToGrab = ["picture", "type", "quantity", "weight", "percent", "value"];
        for (j = 0; j < detailsToGrab.length; j++) {
            var td = document.createElement('TD');
            if(j == 1) {
                td.appendChild(infoAnchor);
            }

            if(j == 0) {
                td = imgTD;
                if(bullionList[i][detailsToGrab[j]] != null) {
                    imgURL = bullionList[i][detailsToGrab[j]];
                    var img = document.createElement("IMG");
                    img.src = imgURL;
                    img.className = "coin_mini";
                    imgDiv.appendChild(img);
                }
            }
            else if(bullionList[i][detailsToGrab[j]] != null) {
                td.appendChild(document.createTextNode(bullionList[i][detailsToGrab[j]]));
            }
            tr.appendChild(td);
        }
        bullionTable.appendChild(tr);
        totalValue += bullionList[i]["value"];
    }

    setTimeout(function(){
         document.getElementById("loader").setAttribute("visibility", "none");
         document.getElementById("total_value").innerHTML = "$" + totalValue;
        }, 500);

    // For searching capability
    var $rows = $('#bullionTableBody tr');
    $('#searchCoins').keyup(function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

    $rows.show().filter(function() {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();

    });
}

    // get coin index from table and grab corresponding coin in array
    function getClickedElement(index) {
        console.log("Index: " + index);
        var coin = formattedResults[index];
        setCoinInfo(coin);
    }

    function setCoinInfo(coin) {
        //"picture", "metal", "type", "purchaseDate", "quantity", "premium", "unitPrice", "percent", "weight", "gu", "ozt", "totalOZT", "value"
        localStorage.setItem("coin_info_coin", JSON.stringify(coin));

        // format of the date in stringify is weird...
        localStorage.setItem("purchase_date_info", coin["purchaseDate"]);

        // go to coinInfo page
        window.location.href = "coinInfo.html";
    }

//only initialized once to tell whether we are in view or edit mode
var isViewMode = true;
function toggleEditViewMode() {
    //toggles view mode
    isViewMode = !isViewMode;

    //flips all of the readonly items to be editable or vice versa
    var inputsTags = document.getElementsByClassName("toggleEditable");
    for (var i = 0; i < inputsTags.length; ++i) {
        inputsTags[i].readOnly = !inputsTags[i].readOnly;
    }

    //Grabs the three buttons that change
    var editCancelButton = document.getElementById("editCancelButton");
    var deleteButton = document.getElementById("deleteButton");
    var saveButton = document.getElementById("save");

    //Set the image and visibility depending on the mode
    if (isViewMode) {
        editCancelButton.src="img/pencil.png";
        editCancelButton.title="Edit";
        deleteButton.style.visibility="hidden";
        saveButton.style.visibility="hidden";
    } else {
        editCancelButton.src="img/cancel.png";
        editCancelButton.title="Cancel";
        deleteButton.style.visibility="visible";
        saveButton.style.visibility="visible";
    }
}

function validateBullionUpdate() {
    var errors = {};
    errors.errorMessage = "";
    var quantity = document.getElementById("qty_info");
    var premium = document.getElementById("premium_info");
    var unitPrice = document.getElementById("unit_info");

    if (quantity.value.length < 1) {
        errors.errorMessage += "Please enter a quantity.\n";
        errors.quantityError = true;
    } else if (isNaN(quantity.value)) {
        errors.errorMessage += "The quantity must be a numeric value.\n";
        errors.quantityError = true;
    } else if (quantity.value < 0) {
        errors.errorMessage += "The quantity must be a positive integer.\n";
        errors.quantityError = true;
    }

    if (premium.value.length < 1) {
        errors.errorMessage += "Please enter a premium.\n";
        errors.premiumError = true;
    } else if (isNaN(premium.value)) {
        errors.errorMessage += "The premium must be a numeric value.\n";
        errors.premiumError = true;
    } else if (premium.value < 0) {
        errors.errorMessage += "The premium must be a positive integer.\n";
        errors.premiumError = true;
    }

    if (unitPrice.value.length < 1) {
        errors.errorMessage += "Please enter a unit price.\n";
        errors.unitPriceError = true;
    } else if (isNaN(unitPrice.value)) {
        errors.errorMessage += "The unit price must be a numeric value.\n";
        errors.unitPriceError = true;
    } else if (unitPrice.value < 0) {
        errors.errorMessage += "The unit price must be a positive number.\n";
        errors.unitPriceError = true;
    }

    if (errors.quantityError) {
        quantity.style.background = '#AA0000';
    } else {
        quantity.style.background = '#4c4c4c';
    }

    if (errors.premiumError) {
        premium.style.background = '#AA0000';
    } else {
        premium.style.background = '#4c4c4c';
    }

    if (errors.unitPriceError) {
        unitPrice.style.background = '#AA0000';
    } else {
        unitPrice.style.background = '#4c4c4c';
    }

    errors.error = errors.quantityError || errors.premiumError || errors.unitPriceError;
    if (errors.error) {
        alert(errors.errorMessage);
    }
    return !errors.error;
}

function updateBullion() {
    if (validateBullionUpdate()) {
        Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
        Parse.User.current().fetch().then(function (user) {
            var userName = user.get("username");
            var Bullion = Parse.Object.extend("Bullion");
            var query = new Parse.Query(Bullion);
            query.equalTo("userName", userName);
            query.equalTo("type", document.getElementById("type_info").innerHTML);
            query.find({
                success: function (results) {
                    var result = results[0];
                    result.set("quantity", parseInt(document.getElementById("qty_info").value));
                    result.set("premium", parseInt(document.getElementById("premium_info").value));
                    result.set("unitPrice", parseInt(document.getElementById("unit_info").value));
                    result.set("value", parseInt(document.getElementById("totalStrong").innerHTML));

                    result.save(null, {
                        success: function (updatedObject) {
                            alert("Update complete!");
                            toggleEditViewMode();
                        },

                        error: function (error) {
                            alert("An error occurred while trying to save you update :,(");
                        }
                    })
                },
                error: function (error) {
                    console.log("Error getting all bullion: " + error.code + " -> " + error.message);
                    return undefined;
                }
            });
        });
    }
}

function destoyBullion() {
    Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
    Parse.User.current().fetch().then(function (user) {
        var userName = user.get("username");
        var Bullion = Parse.Object.extend("Bullion");
        var query = new Parse.Query(Bullion);
        query.equalTo("userName", userName);
        query.equalTo("type", document.getElementById("type_info").innerHTML);
        query.find({
            success: function(results) {
                results[0].destroy({
                   success: function(deadObject) {
                        alert("Successfully deleted the item.");
                        document.location = document.getElementById("backLink").getAttribute("href");
                   },

                    error: function(error) {
                        alert("An error occurred while trying to delete your bullion :,(");
                    }
                });
            },

            error: function(error) {
                alert("An error occurred while trying to delete your bullion :,(");
            }
        });
    });
}

/*
options = {
    metal: String,
    type: String,
    picture: ???,
    purchase: Date,
    premium: Number,
    Unit Price: Number
    }

create Bullion(options, picture)
getBullionForUser(metal)
deleteBullion(type, quarnity)
editBullion(options, picture)
*/

/*
metal: $('#metal').val(),
            type: $('#type').val(),
            purchaseDate: $('#purchaseDate').val(),
            premium: $('#value').val(),
            unitPrice: $('#unitPrice').val()
*/

function createBullion(picFile, email) {
        Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
        var newBullion = Parse.Object.extend("Bullion")
        var newB = new newBullion();
        newB.save({
            userName: email,
            metal: $('#metal').val(),
            type: $('#type').val(),
            quantity: parseInt($('#quantity').val()),
            purchaseDate: checkDate($('#purchaseDate').val()),
            premium: parseFloat($('#value').val()),
            unitPrice: parseFloat($('#unitPrice').val()),
            picture: picFile,
            percent: parseFloat($('#percentValue').text()),
            ozt: parseFloat($('#oztValue').text()),
            total: parseFloat($('#bigTotal').text()),
            weight: parseFloat($('#grams').text()),
            value: parseFloat($('#bigTotal').text()),
            gu: parseFloat($('#totalValue').text()),
            totalOZT: parseFloat($('#oztValue').text())

        },{
        success: function(newB){
            //alert("it went through");
            document.location = $('#metal').val() + "Home.html";
        },
        error: function(newB,error){
            alert('post failed, fields all need to be filled correctly');
        }});
    }

function createBullionNoPic(email) {
    Parse.initialize("ngE7llyYQIad0sL4XWHH7OaiXtu7TFnpYOfT8Exa", "05Eihhyjq45b7AdDqQSGzekR9674u1M419dUsryJ");
    var newBullion = Parse.Object.extend("Bullion")
    var newB = new newBullion();
    newB.save({
        userName: email,
        metal: $('#metal').val(),
        type: $('#type').val(),
        quantity: parseInt($('#quantity').val()),
        purchaseDate: checkDate($('#purchaseDate').val()),
        premium: parseFloat($('#value').val()),
        unitPrice: parseFloat($('#unitPrice').val()),
        percent: parseFloat($('#percentValue').text()),
        ozt: parseFloat($('#oztValue').text()),
        total: parseFloat($('#bigTotal').text()),
        weight: parseFloat($('#grams').text()),
        value: parseFloat($('#bigTotal').text()),
        gu: parseFloat($('#totalValue').text()),
        totalOZT: parseFloat($('#oztValue').text())

    },{
        success: function(newB){
            //alert("it went through");
            document.location = $('#metal').val() + "Home.html";
        },
        error: function(newB,error){
            alert('post failed, fields all need to be filled correctly');
        }});
}


//used for date validation.
function checkDate(str) {

        var matches = str.match(/(\d{1,2})[- \/](\d{1,2})[- \/](\d{4})/);
        if (!matches) return;

        // convert pieces to numbers
        // make a date object out of it
        var month = parseInt(matches[1], 10);
        var day = parseInt(matches[2], 10);
        var year = parseInt(matches[3], 10);
        var date = new Date(year, month - 1, day);
        if (!date || !date.getTime()) return;

        // make sure we didn't have any illegal
        // month or day values that the date constructor
        // coerced into valid values
        if (date.getMonth() + 1 != month ||
            date.getFullYear() != year ||
            date.getDate() != day) {
                return;
            }
    return(date);
    }
